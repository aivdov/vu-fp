module Vupirma
where

import Data.List 

type InternalMap = [(String, String)]
type ExternalMap = [InternalMap]

msg :: String
msg = "List(Map(x -> 0, y -> 1, v -> x), Map(x -> 2, y -> 2, v -> o), Map(x -> 2, y -> 1, v -> x), Map(x -> 0, y -> 1, v -> o))"
decode :: String -> ExternalMap
decode = decodeExternal

decodeExternal :: String -> ExternalMap
decodeExternal ('L':'i':'s':'t':'(' : rest) =
    readFullExternalMap rest []
decodeExternal _ = error "no map"


readFullExternalMap :: String -> ExternalMap -> ExternalMap
readFullExternalMap [] acc = acc
readFullExternalMap (',':t) acc = readFullExternalMap t acc
readFullExternalMap (' ':t) acc = readFullExternalMap t acc
readFullExternalMap (')':[]) acc = readFullExternalMap "" acc
readFullExternalMap str acc =
    let
        --(key, left) = readTillComma str
        (val, left) = readInternalMap str
    in readFullExternalMap left (val : acc)

readKeyVal :: String -> (String, String)
readKeyVal (' ':rest) = readKeyVal rest
readKeyVal kvpair = 
    let 
        key = takeWhile (/= ' ') kvpair
        value = drop (length key + 4) kvpair
    in (key, value)

readTillComma :: String -> (String, String)
readTillComma pair =
    let 
        key = takeWhile (/= ',') pair
        rest = drop (length key + 1) pair
    in (key, rest)

readInternalMap :: String -> (InternalMap, String)
readInternalMap ('M':'a':'p':'(' : rest) =
    let
        m = takeWhile (/= ')') rest
        left = drop (length m + 1) rest
        vals = reverse $ readFullInternalMap m []
    in (vals, left)

readFullInternalMap :: String -> InternalMap -> InternalMap
readFullInternalMap [] acc = acc
readFullInternalMap str acc =
    let
        (keyval, left) = readTillComma str
        (key, val) = readKeyVal keyval
    in readFullInternalMap left ((key, val) : acc)
    
validate :: String -> Bool
validate string = 
    let
        decodedList = decode string
    in validateList decodedList []

validateList :: ExternalMap -> ExternalMap -> Bool
validateList [] currentMoves = True
validateList remainingList currentMoves = 
    let
        currentMove = remainingList !! 0
        remainingMoves = drop 1 remainingList
        validation = isMoveValid currentMove currentMoves 0
        
        
    in if (validation == True) 
        then validateList remainingMoves (currentMove:currentMoves)
        else False

isMoveValid :: InternalMap -> ExternalMap -> Int -> Bool
isMoveValid currentMove [] acc = True
isMoveValid currentMove (h:t) acc =
    if (acc == 0)
        then if (lookup "v" currentMove == lookup "v" h)
            then False
            else isMoveValid currentMove (h:t) (acc+1)
        else if (lookup "x" currentMove == lookup "x" h) 
                then if (lookup "y" currentMove == lookup "y" h) 
                    then False
                    else isMoveValid currentMove t (acc+1)
                else isMoveValid currentMove t (acc+1)











